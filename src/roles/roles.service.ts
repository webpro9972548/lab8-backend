import { Injectable } from '@nestjs/common';
import { CreateRoleDto } from './dto/create-role.dto';
import { UpdateRoleDto } from './dto/update-role.dto';
import { Role } from './entities/role.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class RolesService {
  constructor(
    @InjectRepository(Role) private roleRepository: Repository<Role>,
  ) {}
  create(createRoleDto: CreateRoleDto) {
    return this.roleRepository.save(createRoleDto);
  }

  findAll() {
    return this.roleRepository.find();
  }

  findOne(id: number) {
    return this.roleRepository.findOneBy({ id });
  }

  async update(id: number, updateRoleDto: UpdateRoleDto) {
    await this.roleRepository.findOneByOrFail({ id });
    await this.roleRepository.update(id, updateRoleDto);
    const updated = await this.roleRepository.findOneBy({ id });
    return updated;
  }

  async remove(id: number) {
    const removed = await this.roleRepository.findOneByOrFail({ id });
    await this.roleRepository.remove(removed);
    return removed;
  }
}
