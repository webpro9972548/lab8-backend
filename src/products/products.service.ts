import { Injectable } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product) private productRepository: Repository<Product>,
  ) {}
  create(createProductDto: CreateProductDto) {
    return this.productRepository.save(createProductDto);
  }

  findAll() {
    return this.productRepository.find();
  }

  findOne(id: number) {
    return this.productRepository.findOneBy({ id });
  }

  async update(id: number, updateProductDto: UpdateProductDto) {
    await this.productRepository.findOneByOrFail({ id });
    await this.productRepository.update(id, updateProductDto);
    const updateProduct = await this.productRepository.findOneBy({ id });
    return updateProduct;
  }

  async remove(id: number) {
    const removed = await this.productRepository.findOneByOrFail({ id });
    await this.productRepository.remove(removed);
    return removed;
  }
}
